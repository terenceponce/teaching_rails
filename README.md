# Teaching Rails

Teaching material based on Agile Web Development with Rails 5

## Usage

This project uses [Remarker](https://github.com/kt3k/remarker) to build the slideshow presentation.

Remarker uses [Remark](https://github.com/gnab/remark) to actually build the slides. Remarker is just there to make things easier.

### Installation

```
$ git clone git@gitlab.com:terenceponce/teaching_rails.git
$ cd teaching_rails
$ npm install
```

### Building a chapter and viewing it in the browser

```
$ cd path/to/chapter/root
$ ../../node_modules/.bin/remarker build
$ open build/index.html
```
