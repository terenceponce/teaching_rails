name: inverse
layout: true
class: center, middle, inverse
---
# Chapter 2: Instant Gratification
---
## New Commmand: rails new
---
## Why not just create everything from scratch?

???
We can just do it, actually. It's just that Rails does a lot of the
heavy lifting to make our applications work with the minimum amount
of explicit configuration.

---
layout: false
.left-column[
  ## Creating a New Application
]
.right-column[
  Use rails new:

  ```shell
  $ rails new demo
  create
  create README.md
  create Rakefile
  create config.ru
  :   :   :
  create vendor/assets/stylesheets
  create vendor/assets/stylesheets/.keep
     run  bundle install
  Fetching gem metadata from https://rubygems.org/...........
  :   :   :
  Bundle complete! 15 Gemfile dependencies, 63 gems now installed.
  Use `bundle show [gemname]` to see where a bundled gem is installed.
           run  bundle exec spring binstub --all
  * bin/rake: spring inserted
  * bin/rails: spring inserted
  ```

  Try making it run:

  ```shell
  $ bin/rails server
  ```

]

---
![image](../images/rails_new_browser.png)
---
template: inverse
## It works!

???
As we explore further in our lessons, Rails is a Model-View-Controller (MVC) framework.
Rails accepts incoming requests from a browser, decodes the request to find a controller,
and calls an action method in that controller.

The controller then invokes a particular view to display the results to the user.

---
template: inverse
# Generating Controllers
---
template: inverse
## New command: rails generate

???
In the same way we used the rails command to create a new Rails application,
we can also use a generator script to create a new controller for our project.

---
.left-column[
  ## Creating a New Application
]
.right-column[

  ```shell
  $ bin/rails generate controller Say hello goodbye
  create app/controllers/say_controller.rb
   route  get "say/goodbye"
   route  get "say/hello"
  invoke  erb
  create    app/views/say
  create    app/views/say/hello.html.erb
  create    app/views/say/goodbye.html.erb
  invoke  test_unit
  create    test/controllers/say_controller_test.rb
  invoke  helper
  create    app/helpers/say_helper.rb
  invoke    test_unit
  create      test/helpers/say_helper_test.rb
  invoke  assets
  invoke    coffee
  create      app/assets/javascripts/say.coffee
  invoke    scss
  create      app/assets/stylesheets/say.scss
  ```

]

???
The rails generate command logs the files and directories it examines,
noting when it adds new Ruby scripts or directories to our application.

---
.left-column[
  ## Our First Rails Controller
]
.right-column[
  ```ruby
  class SayController < ApplicationController
    def hello
    end

    def goodbye
    end
  end
  ```
]

???
SayController is a class that inherits from ApplicationController, so it
automatically gets all of the default controller behavior.

To understand why these methods are named this way, we need to look at
the way Rails handles requests.

---
template: inverse
## Rails and Request URLs
---
.left-column[
  ## Rails and Request URLs
]
.right-column[
  Fire up `rails server` and go to http://localhost:3000/say/hello

  ![image](../images/say_hello_first.png)
]

???
At this point, we can see not only that we’ve connected the URL to our
controller but also that Rails is pointing the way to our next step—namely,
to tell Rails what to display. That’s where views come in.

By default, Rails looks for templates in a file with the same name as the
action it’s handling. In our case, that means we need to replace a file called
hello.html.erb in the app/views/say directory.

---
.left-column[
  ## Rails and Request URLs
]
.right-column[
  Go to `app/views/say/hello.html.erb`:

  In ERB:

  ```html
  <h1>Hello from Rails!</h1>
  ```


  In HAML:


  ```haml
  %h1 Hello from Rails!
  ```

  Will output:

  ![image](../images/say_hello_second.png)

]

---
.left-column[
  ## Rails and Request URLs
]
.right-column[
  ![image](../images/say_hello_directory.png)
]

???
In total, we’ve looked at two files in our Rails application tree.
We looked at the controller, and we modified a template to display
a page in the browser. These files live in standard locations in the
Rails hierarchy: controllers go into app/controllers, and views go
into subdirectories of app/views. You can see this structure in the diagram.

---
template: inverse
# Making things dynamic
---
template: inverse
## Let's have the page show the current time

???
To do this, we need to change the template file in the view—it now needs to include
the time as a string. That raises two questions. First, how do we add dynamic content
to a template? Second, where do we get the time from?

---
template: inverse
# <%= %> and <% %>
---
.left-column[
  ## Dynamic Content
]
.right-column[
  Change the to display the current time:

  In ERB (`hello.html.erb`):

  ```html
  <h1>Hello from Rails!</h1>
  <p>
    It is now <%= Time.now %>
  </p>
  ```

  In HAML (`hello.html.haml`):

  ```haml
  %h1 Hello from Rails!
  %p It is now #{Time.now}
  ```

]

---
template: inverse
![image](../images/say_hello_third.png)
---
template: inverse
# Rails makes development easier

???
You might have noticed something about the development we’ve been doing so far.
As we’ve been adding code to our application, we haven’t had to restart the running
application. It’s been happily chugging away in the background. And yet each change
we make is available whenever we access the application through a browser. What gives?

---
template: inverse
# Rails automatically reloads application source files

???
It turns out that the Rails dispatcher is pretty clever. In development mode
(as opposed to testing or production), it automatically reloads application source
files when a new request comes along. That way, when we edit our application,
the dispatcher makes sure it’s running the most recent changes. This is great for development.

---
template: inverse
# Autoreloading is disabled in Production-like environments

???
However, this flexibility comes at a cost: it causes a short pause after you enter a URL
before the application responds. That’s caused by the dispatcher reloading stuff. For
development it’s a price worth paying, but in production it would be unacceptable.
For this reason, this feature is disabled for production deployment.

---
template: inverse
# Improving the code we've written
---
template: inverse
# Views are meant to do one thing: Displaying things

???
We’ve shown that the approach of embedding a call to Ruby’s Time.now() method in
our hello.html.erb template works.

Each time they access this page, users will see the current time substituted into
the body of the response. And for our trivial application, that might be good enough.

In general, though, we probably want to do something slightly different.

We’ll move the determination of the time to be displayed into the controller and
leave the view with the simple job of displaying it.

We’ll change our action method in the controller to set the time value into an
instance variable called @time.

---
.left-column[
  ## Refactoring our code
]
.right-column[
  Open `app/controllers/say_controller.rb`:

  ```ruby
  class SayController < ApplicationController
    def hello
      @time = Time.now
    end

    def goodbye
    end
  end
  ```

  In our view `app/views/say/hello.html.erb` (ERB):

  ```html
  <h1>Hello from Rails!</h1>
  <p>
    It is now <%= @time %>
  </p>
  ```


  In our view `app/views/say/hello.html.haml` (HAML):


  ```haml
  %h1 Hello from Rails!
  %p It is now #{@time}
  ```

]

---
template: inverse
# Why did we even do this?

???
Why did we go to the extra trouble of setting the time to be displayed in the
controller and then using it in the view? Good question.

In this application, it doesn’t make much difference, but by putting the logic
in the controller instead, we buy ourselves some benefits.

For example, we may want to extend our application in the future to support users
in many countries. In that case, we’d want to localize the display of the time,
choosing a time appropriate to the user’s time zone.

That would require a fair amount of application-level code, and it would probably
not be appropriate to embed it at the view level.

By setting the time to display in the controller, we make our application more flexible:
we can change the time zone in the controller without having to update any view that uses
that time object. The time is data, and it should be supplied to the view by the controller.

We’ll see a lot more of this when we introduce models into the equation.

---
template: inverse
# The story so far
---
.left-column[
  ## The story so far
]
.right-column[
  How our application works:

  - The user navigates to our application. In our case, we do that using a local URL such as http://localhost:3000/say/hello.
  - Rails then matches the route pattern, which it previously split into two parts and analyzed. The say part is taken to be the name of a controller, so Rails creates a new instance of the Ruby SayController class (which it finds in app/controllers/say_controller.rb).
  - The next part of the pattern, hello, identifies an action.  Rails invokes a method of that name in the controller.  This action method creates a new Time object holding the current time and tucks it away in the @time instance variable.
]
---
.left-column[
  ## The story so far
]
.right-column[
  How our application works:

  - Rails looks for a template to display the result.  It searches the app/views directory for a subdirectory with the same name as the controller (say) and in that subdirectory for a file named after the action (hello.html.erb).
  - Rails processes this file through the ERB templating system, executing any embedded Ruby and substituting in values set up by the controller.
  - The result is returned to the browser, and Rails finishes processing this request.
]

---
template: inverse
# Linking Pages Together
---
.left-column[
  ## Linking Pages Together
]
.right-column[
  Open up the Goodbye view in your editor.

  In ERB (`app/views/say/goodbye.html.erb`):

  ```html
  <h1>Goodbye!</h1>
  <p>
    It was nice having you here.
  </p>
  ```

  In HAML (`app/views/say/goodbye.html.haml`):

  ```haml
  %h1 Goodbye!
  %p It was nice having you here.
  ```

]

---
![image](../images/say_goodbye_first.png)
---
template: inverse
# Actually linking the pages together

???
Now we need to link the two screens. We’ll put a link on the hello screen that
takes us to the goodbye screen, and vice versa.

In a real application, we might want to make these proper buttons, but for now we’ll use hyperlinks.

We already know that Rails uses a convention to parse the URL into a target controller
and an action within that controller.

So, a simple approach would be to adopt this URL convention for our links.

---
.left-column[
  ## Actually linking the pages together
]
.right-column[
  Open our Hello view:

  In ERB (`app/views/say/hello.html.erb`):


  ```html
  ...
  <p>
    Say <a href="/say/goodbye">Goodbye</a>!
  </p>
  ```

  In HAML (`app/views/say/hello.html.haml`):


  ```haml
  %p
    Say
    %a{href: "/say/goodbye"} Goodbye
    !
  ```

]
---
.left-column[
  ## Actually linking the pages together
]
.right-column[
  Open our Goodbye view:

  In ERB (`app/views/say/goodbye.html.erb`):


  ```html
  ...
  <p>
    Say <a href="/say/hello">Hello</a>!
  </p>
  ```

  In HAML (`app/views/say/goodbye.html.haml`):


  ```haml
  %p
    Say
    %a{href: "/say/hello"} Hello
    !
  ```

]

---
template: inverse
## Our solution works but it's fragile

???
This approach would certainly work, but it’s a bit fragile.
If we were to move our application to a different place on the web server,
the URLs would no longer be valid.

It also encodes assumptions about the Rails URL format into our code;
it’s possible a future version of Rails could change that format.

---
template: inverse
## link_to()

???
Fortunately, these aren’t risks we have to take.

Rails comes with a bunch of helper methods that can be used in view templates.

Here, we’ll use the link_to() helper method, which creates a hyperlink to an action.

(The link_to() method can do a lot more than this, but let’s take it gently for now.)

Using link_to(), hello.html.erb becomes the following:

---
.left-column[
  ## Refactoring our code
]
.right-column[
  Open the Hello view.

  In ERB (`app/views/say/hello.html.erb`):


  ```html
  <h1>Hello from Rails!</h1>
  <p>
    It is now <%= @time %> </p>
  <p>
    Time to say
    <%= link_to "Goodbye", say_goodbye_path %>!
  </p>
  ```


  In HAML (`app/views/say/hello.html.haml`):

  ```haml
  %h1 Hello from Rails!
  %p It is now #{@time}
  %p
    Time to say
    = link_to "Goodbye", say_goodbye_path
    !
  ```

]

???
There’s a link_to() call within an ERB <%=...%> sequence.

This creates a link to a URL that will invoke the goodbye() action.

The first parameter in the call to link_to() is the text to be displayed
in the hyperlink, and the next parameter tells Rails to generate the link to the goodbye() action.

---
template: inverse
## link_to "Goodbye", say_goodbye_path

???
Let’s stop for a minute to consider how we generated the link.

First, link_to() is a method call. (In Rails, we call methods that make it easier to write templates helpers.)

If you come from a language such as Java, you might be surprised that Ruby doesn’t
insist on parentheses around method parameters. You can always add them if you like.

say_goodbye_path is a precomputed value that Rails makes available to application views.

It evaluates to the /say/goodbye path. Over time, you’ll see that Rails provides the
ability to name all the routes that you use in your application.

---
.left-column[
  ## link_to()
]
.right-column[
  Change the goodbye view to link back to hello:

  In ERB (`app/views/say/goobye.html.erb`):


  ```html
  <h1>Goodbye!</h1>
  <p>
    It was nice having you here.
  </p>
  <p>
    Say <%= link_to "Hello", say_hello_path %> again.
  </p>
  ```


  In HAML (`app/views/say/goobye.html.haml`):


  ```haml
  %h1 Goodbye!
  %p It was nice having you here.
  %p Say #{link_to "Hello", say_hello_path} again.
  ```

]

---
template: inverse
# Everything works!
---
template: inverse
# What if things go wrong?

???
So far, we’ve just done things that should work, and—unsurprisingly—they’ve worked.
But the true test of the developer friendliness of a framework is how it responds when things go wrong.

As we’ve not invested much time into this code yet, now is a perfect time to try to break things.

---
.left-column[
  ## When Things Go Wrong
]
.right-column[
  Edit `app/controllers/say_controller.rb` and let's introduce a typo in it:

  ```ruby
  class SayController < ApplicationController
    def hello
      @time = Time.know # Put a typo here
    end

    def goodbye
    end
  end
  ```
]

---
![image](../images/say_hello_error.png)
---
template: inverse
# What if we put an invalid URL?
---
![image](../images/say_hello_route_error.png)
---
template: inverse
# Summarizing what we just did
---
.left-column[
  ## Summary
]
.right-column[
  We constructed a toy application that showed you the following:

  - How to create a new Rails application and how to create a new controller in that application
  - How to create dynamic content in the controller and display it via the view template
  - How to link pages together
  - How to debug problems in the code or the URL
]

???
This is a great foundation, and it didn’t take much time or effort.
This experience will continue as we move on to the next chapter and build a much bigger application.
