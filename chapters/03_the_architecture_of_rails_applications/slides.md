name: inverse
layout: true
class: center, middle, inverse
---
# Chapter 3: The Architecture of Rails Applications
---
# Rails is an MVC framework
---
# Model
---
layout: false
.left-column[
  ## Model-View-Controller
]
.right-column[
  A Model is responsible for the following things:

  - Maintaining the state of the application
  - Enforces all of the business rules
  - Acts as a gatekeeper and a data store
]

---
template: inverse
# View
---
.left-column[
  ## Model-View-Controller
]
.right-column[
  A View is responsible for the following things:

  - Generating user interfaces (sometimes based on data from the model layer)

  What it doesn't do:

  - Handle data

]

---
template: inverse
# Controller
---
.left-column[
  ## Model-View-Controller
]
.right-column[
  A Controller is responsible for the following things:

  - Orchestrates the application
  - Receives events from the outside world
  - Interact with the model
  - Interact with the view

  What it doesn't do:

  - Model things
  - View things

]

---
template: inverse
# MVC was originally intended for Desktop GUI applications

---
template: inverse
# Developers found that separation of concerns led to far less coupling

???
Which, in turn, made it easier to write and maintain code

---
template: inverse
# Rails imposes some fairly serious constraints about how we structure our web applications

???
Rails enforces a structure for your application: you develop models, views, and
controllers as separate chunks of functionality, and it knits them together as
your program executes.

---
template: inverse
# Rails favors convention over configuration
---
template: inverse
# How Rails implements MVC

???
Browser > Router > Controller > Model > Database >
Controller > View > Browser

The routing component receives the incoming request and immediately
picks it apart. The request contains a path (/line_items?product_id=2)
and a method (this button does a POST operation; other common methods are
GET, PUT, PATCH, and DELETE). In this simple case, Rails takes the first part
of the path, line_items, as the name of the controller and the product_id as the
ID of a product. By conven- tion, POST methods are associated with create() actions.
As a result of all this analysis, the router knows it has to invoke the create() method
in the LineItemsController controller class.

---
template: inverse
# If MVC is simply a question of partitioning your code in a certain way, why use Rails?

???
Rails handles all of the low-level housekeeping for you
all those messy details that take so long to handle by yourself
and lets you concentrate on your application’s core functionality.

---
template: inverse
# Rails Model Support

???
In general, we want our web applications to keep their information in a relational database.

---
template: inverse
# Problem
---
template: inverse
# Databases are all about sets of values
---
template: inverse
# Objects are all about data and operations

???
Operations that are easy to express in relational terms are sometimes difficult
to code in an OO system. The reverse is also true.

Over time, folks have worked out ways of reconciling the relational and OO views of
their corporate data. Let’s look at the way that Rails chooses to map relational data onto objects.

---
template: inverse
# Solution: Object-Relational Mapping

???
Object-relational mapping (ORM) libraries map database tables to classes.
If a database has a table called orders, our program will have a class named Order.
Rows in this table correspond to objects of the class—a particular order is represented
as an object of the Order class. Within that object, attributes are used to get and set
the individual columns. Our Order object has methods to get and set the amount, the sales tax, and so on.

---
template: inverse
# Rails provides a set of methods that perform database-level operations
---
.left-column[
  ## Active Record
]
.right-column[
  Some sample Ruby code for interacting with the database:

  ```ruby
  order = Order.find(1)
  puts "Customer #{order.customer_id}, amount=$#{order.amount}"
  ```

  Class-level methods that return collections of objects:

  ```ruby
  Order.where(name: 'dave').each do |order|
    puts order.amount
  end
  ```

  Instance-level methods that interact with the database row:

  ```ruby
  Order.where(name: 'dave').each do |order|
    order.pay_type = "Purchase order"
    order.save
  end
  ```

]

---
template: inverse
# An ORM layer maps tables to classes, rows to objects, and columns to attributes of those objects
---
template: inverse
# Class methods are used to perform table-level operations, and instance methods perform operations on the individual rows
---
template: inverse
# Active Record

???
Active Record is the ORM layer supplied with Rails.
It closely follows the standard ORM model: tables map to classes,
rows to objects, and columns to object attributes. It differs from most
other ORM libraries in the way it’s configured. By relying on convention
and starting with sensible defaults, Active Record minimizes the amount
of configuration that developers perform.

---
.left-column[
  ## Active Record
]
.right-column[
  Sample Active Record class:

  ```ruby
  require 'active_record'

  class Order < ActiveRecord::Base
  end
  ```

  ```ruby
  order = Order.find(1)
  order.pay_type = "Purchase order"
  order.save
  ```
]

---
template: inverse
# Action Pack: The View and Controller
---
template: inverse
# One component for the rest of the layers?

???
When you think about it, the view and controller parts of MVC are pretty intimate.
The controller supplies data to the view, and the controller receives events from the
pages generated by the views. Because of these interactions, support for views and
controllers in Rails is bundled into a single component, Action Pack
