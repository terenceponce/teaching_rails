name: inverse
layout: true
class: center, middle, inverse
---
# Chapter 1: Installing Rails
---
layout: false
.left-column[
  ## What do we need?
]
.right-column[
  In order to get Rails running, we need the following:

  - A Ruby Interpreter
  - Ruby on Rails
  - A Javascript Interpreter
  - Some System Libraries
  - A Database
  - A Text Editor
]
---
template: inverse
# Installing on Windows
---
template: inverse
## DON'T DO IT

???
Ruby development on Windows is not usually recommended
because of the amount of pain that you'll encounter
along the way.

It's just easier to do Ruby in a UNIX-based Operating System.

---
.left-column[
  ## Installing on Windows
]
.right-column[
  ![image](../images/rubyinstaller.png)
]

???
If you must do it in a Windows machine, RubyInstaller has proven
to be the easiest way to install Ruby on Windows.

Proceed at your own risk.

---
template: inverse
# Installing on Mac OS X

???
Mac OS X is the ideal operating system for Ruby developers.

Mac OS X combines the features a developer would look for in Linux
with the user-friendliness of Windows.

---
.left-column[
  ## Installing on Mac OS X
]
.right-column[
  ![image](../images/xcode.png)
]

???
XCode is needed because it contains most of the software libraries
needed to build/compile Ruby and most of the tools relating to it.

---
template: inverse
## rbenv
---
template: inverse
## RVM
---
.left-column[
  ## Using rbenv
]
.right-column[
  ![image](../images/rbenv.png)
]
---
template: inverse
## Install Homebrew
---
.left-column[
  ## Using rbenv
]
.right-column[
  Go to https://brew.sh and execute the line below:

  ```shell
  /usr/bin/ruby -e
  "$(curl -fsSL
  https://raw.githubusercontent.com
  /Homebrew/install/master/install)"
  ```

  Afterwards:

  ```shell
  $ brew update
  $ brew install rbenv
  $ brew install ruby-build
  $ rbenv init
  $ rbenv install 2.4.1
  $ rbenv global 2.4.1
  ```

]
---
.left-column[
  ## Using RVM
]
.right-column[
  ![image](../images/rvm.png)
]
---
.left-column[
  ## Using RVM
]
.right-column[
  Go to https://rvm.io and execute the line below:

  ```shell
  $ \curl -sSL https://get.rvm.io | bash -s stable
  ```

  Afterwards:

  ```shell
  $ rvm install 2.4.1
  $ rvm --default use 2.4.1
  ```

]
---
template: inverse
# Installing on Linux / Ubuntu
---
template: inverse
## Just use RVM or rbenv
---
template: inverse
# Installing Rails
---
.left-column[
  ## Installing Rails
]
.right-column[
  Open your Command Line / Terminal

  ```shell
  $ gem install bundler --no-ri --no-rdoc
  $ gem install rails --no-ri --no-rdoc
  ```

]
---
template: inverse
# Getting To Know Your Development Environment
---
![image](../images/dev_environment.png)
---
.left-column[
  ## Setting Up
]
.right-column[
  Typical Developer Setup:

  - Command Line
  - Version Control
  - Browser
  - Editor
]
---
template: inverse
# Command Line
---
template: inverse
## Our main tool and where a lot of the work happens.
---
template: inverse
## Despite the increasing number of GUI tools
## that help generate and manage and application,
## the Command Line is still king.
---
template: inverse
# Version Control
---
template: inverse
## A Version Control System keeps track of our work
---
template: inverse
## It makes collaborating with other people a lot easier
---
template: inverse
## An important part of setting up a Continuous Integration (CI) System

???
It's important because all CI systems usually retrieve a copy of the codebase
and perform the tests on said copy.

---
template: inverse
## Git is the most popular Version Control Software (VCS) today
---
template: inverse
# Text Editor
---
template: inverse
## This is where we write our code
---
template: inverse
## There is no One-True-Editor

???
Despite what some people say, it doesn't matter what editor you choose to use
for Ruby development as long as you adhere to the conventions of the technology
or the people you're working with.

---
.left-column[
  ## Things To Consider
]
.right-column[
  These are some things that you have to consider when choosing an editor:

  - Support for syntax highlighting of Ruby and HTML / ERB
  - Support for automatic indentation / reindentation of Ruby source
  - Good file navigation
  - Support for insertion of Ruby / Rails constructs
  - Name completion
  - Bonus: Support for refactoring
]
---
.left-column[
  ## Things To Consider
]
.right-column[
  Here are some recommendations for text editors that you can use in Ruby:

  - Atom (https://atom.io)
  - TextMate (https://macromates.com/)
  - Sublime Text (https://www.sublimetext.com/)
  - Visual Studio Code (https://code.visualstudio.com/)


  For IDEs:


  - RubyMine (https://www.jetbrains.com/ruby/)
  - Aptana Studio 3 (http://www.aptana.com/)
  - jEdit (http://www.jedit.org/)
  - Komodo (https://www.activestate.com/komodo-edit)
  - NetBeans (https://netbeans.org/)
]
---
template: inverse
## 2 Spaces Indentation / Spaces as Tabs

???
This is the simplest rule that we have for Ruby development.

---
template: inverse
# Databases
---
.left-column[
  ## Choosing a Database
]
.right-column[
  Common Database Choices:

  - SQLite
  - MySQL
  - PostgreSQL
]
---
.left-column[
  ## SQLite
]
.right-column[
  Pros:

  - Built into Rails
  - Embedded / Portable


  Cons:


  - Limited functionality
  - Not meant for production-level applications
]
---
.left-column[
  ## MySQL
]
.right-column[
  Pros:

  - Most popular database for a long time now
  - Production-ready
  - Easy to learn


  Cons:


  - Not completely Open Source Software anymore (No thanks to Oracle)
  - Lacks in some features compared to PostgreSQL
]
---
.left-column[
  ## PostgreSQL
]
.right-column[
  Pros:

  - Most popular option in the Rails community
  - Fully Open Source
  - Production-ready
  - Fully-featured compared to most databases today


  Cons:


  - Difficult to setup
  - High learning curve for beginners
  - Uses proprietary SQL extensions that don't exist in other databases
  - In some cases, PostgreSQL is slower than MySQL
]
---
template: inverse
## You're ready to build an application now!
